#pragma once
#include <radspa.h>
#include <radspa_helpers.h>

extern radspa_descriptor_t trigger_merge_desc;
radspa_t * trigger_merge_create(uint32_t init_var);
void trigger_merge_run(radspa_t * osc, uint16_t num_samples, uint32_t render_pass_id);

