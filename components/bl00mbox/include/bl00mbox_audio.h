//SPDX-License-Identifier: CC0-1.0
#pragma once
#include "bl00mbox_config.h"
#include "bl00mbox_os.h"

#include <stdio.h>
#include <math.h>
#include <string.h>
#include "radspa.h"
#include "radspa_helpers.h"

struct _bl00mbox_bud_t;
struct _bl00mbox_connection_source_t;
struct _bl00mbox_channel_root_t;
struct _bl00mbox_channel_t;

extern int16_t * bl00mbox_line_in_interlaced;

typedef struct _bl00mbox_bud_t{
    radspa_t * plugin; // plugin
    char * name;
    uint64_t index; // unique index number for bud
    uint32_t render_pass_id; // may be used by host to determine whether recomputation is necessary
    uint32_t init_var; // init var that was used for plugin creation
    int32_t channel; // index of channel that owns the plugin
    volatile bool is_being_rendered; // true if rendering the plugin is in progress, else false.
    bool always_render;
    struct _bl00mbox_bud_t * chan_next; //for linked list in bl00mbox_channel_t
} bl00mbox_bud_t;

typedef struct _bl00mbox_bud_list_t{
    struct _bl00mbox_bud_t * bud;
    struct _bl00mbox_bud_list_t * next;
} bl00mbox_bud_list_t;

typedef struct _bl00mbox_connection_subscriber_t{
    uint8_t type; // 0: standard signal input, 1: output mixer
    int32_t channel;
    uint64_t bud_index;
    uint32_t signal_index;
    struct _bl00mbox_connection_subscriber_t * next;
} bl00mbox_connection_subscriber_t;

typedef struct _bl00mbox_connection_t{ //child of bl00mbox_ll_t
    int16_t buffer[BL00MBOX_MAX_BUFFER_LEN]; // MUST stay on top of struct bc type casting!
    struct _bl00mbox_bud_t * source_bud;
    uint32_t signal_index; // signal of source_bud that renders to buffer
    struct _bl00mbox_connection_subscriber_t * subs;
    int32_t channel;
    struct _bl00mbox_connection_t * chan_next; //for linked list in bl00mbox_channel_t;
} bl00mbox_connection_t;

typedef struct _bl00mbox_channel_root_t{
    struct _bl00mbox_connection_t * con;
    struct _bl00mbox_channel_root_t * next;
} bl00mbox_channel_root_t;

typedef struct{
    int32_t index;
    bl00mbox_lock_t render_lock;
    bool is_active; // rendering can be skipped if false
    bool compute_mean_square;
    uint32_t mean_square;
    char * name;
    int32_t volume;
    int32_t sys_gain;
    int32_t dc;
    struct _bl00mbox_channel_root_t * root_list; // list of all roots associated with channels
    uint32_t render_pass_id; // may be used by host to determine whether recomputation is necessary
    struct _bl00mbox_bud_t * buds; // linked list with all channel buds
    struct _bl00mbox_bud_list_t * always_render; // linked list of buds that should always render
    struct _bl00mbox_connection_t * connections; // linked list with all channel connections
} bl00mbox_channel_t;

void bl00mbox_audio_init();

bl00mbox_channel_t * bl00mbox_get_channel(int32_t chan);
void bl00mbox_channel_enable(int32_t chan);

void bl00mbox_channel_enable(int32_t chan);
void bl00mbox_channel_disable(int32_t chan);
void bl00mbox_channel_set_volume(int32_t chan, uint16_t volume);
int16_t bl00mbox_channel_get_volume(int32_t chan);
void bl00mbox_channel_set_sys_gain(int32_t chan, int16_t volume);
int16_t bl00mbox_channel_get_sys_gain(int32_t chan);
void bl00mbox_channel_set_compute_mean_square(int32_t chan, bool compute);
bool bl00mbox_channel_get_compute_mean_square(int32_t chan);
uint32_t bl00mbox_channel_get_mean_square(int32_t chan);
void bl00mbox_channel_event(int32_t chan);
bool bl00mbox_channel_get_free(int32_t channel_index);
bool bl00mbox_channel_set_free(int32_t channel_index, bool free);

bool bl00mbox_channel_get_background_mute_override(int32_t channel_index);
bool bl00mbox_channel_set_background_mute_override(int32_t channel_index, bool enable);

char * bl00mbox_channel_get_name(int32_t channel_index);
void bl00mbox_channel_set_name(int32_t channel_index, char * new_name);

void bl00mbox_audio_bud_render(bl00mbox_bud_t * bud);

bool bl00mbox_get_channel_exists(int32_t channel_index);
int32_t bl00mbox_channel_get_free_index();
int32_t bl00mbox_get_channel_index_positional_all_chans(int32_t position);
int32_t bl00mbox_get_channel_index_positional_background_mute_override_chans(int32_t position);
int32_t bl00mbox_channel_get_foreground_index();
void bl00mbox_channel_set_foreground_index(int32_t channel_index);
