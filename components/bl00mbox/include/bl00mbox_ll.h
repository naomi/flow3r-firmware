#pragma once
#include "bl00mbox_os.h"

typedef struct _bl00mbox_ll_t {
    struct _bl00mbox_ll_t * next;
    void * content;
} bl00mbox_ll_t;

bool bl00mbox_ll_contains(bl00mbox_ll_t ** startref, void * content){
    if(!startref) return false; // list doesn't exist
    bl00mbox_ll_t * seek = * startref;
    if(!seek) return false; // list is empty
    while(seek){
        if(seek->content == content) break;
        seek = seek->next;
    }
    return seek;
}

bool bl00mbox_ll_prepend(bl00mbox_ll_t ** startref, void * content, bl00mbox_lock_t * write_lock){
    if(!startref) return false;
    if(bl00mbox_ll_contains(startref, content)) return false;
    bl00mbox_ll_t * ll = malloc(sizeof(bl00mbox_ll_t));
    if(!ll) return false;
    ll->content = content;
    ll->next = *startref;
    bl00mbox_take_lock(write_lock);
    *startref = ll;
    bl00mbox_give_lock(write_lock);
    return true;
}

bool bl00mbox_ll_pop(bl00mbox_ll_t ** startref, void * content, bl00mbox_lock_t * write_lock){
    if(!startref) return false; // list doesn't exist
    bl00mbox_ll_t * seek = * startref;
    if(!seek) return false; // list is empty
    bl00mbox_ll_t * prev = NULL;
    while(seek){
        if(seek->content == content) break;
        prev = seek;
        seek = seek->next;
    }
    if(!seek) return false; // not found
    bl00mbox_take_lock(write_lock);
    if(prev){ // normal
        prev->next = seek->next;
    } else { // first element
        * startref = seek->next;
    }
    bl00mbox_give_lock(write_lock);
    free(seek);
    return true;
}

