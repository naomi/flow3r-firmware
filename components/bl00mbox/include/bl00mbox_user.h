//SPDX-License-Identifier: CC0-1.0
#pragma once

#include <stdio.h>
#include <math.h>
#include <string.h>

#include "bl00mbox_plugin_registry.h"
#include "bl00mbox_audio.h"
#include "bl00mbox_os.h"

#include <stdint.h>
#include "bl00mbox_audio.h"
#include "radspa_helpers.h"

uint16_t bl00mbox_channel_buds_num(int32_t channel);
uint64_t bl00mbox_channel_get_bud_by_list_pos(int32_t channel, uint32_t pos);
uint16_t bl00mbox_channel_conns_num(int32_t channel);
uint16_t bl00mbox_channel_mixer_num(int32_t channel);
uint64_t bl00mbox_channel_get_bud_by_mixer_list_pos(int32_t channel, uint32_t pos);
uint32_t bl00mbox_channel_get_signal_by_mixer_list_pos(int32_t channel, uint32_t pos);
bool bl00mbox_channel_clear(int32_t channel);

bool bl00mbox_channel_connect_signal_to_output_mixer(int32_t channel, uint32_t bud_index, uint32_t bud_signal_index);
bool bl00mbox_channel_connect_signal(int32_t channel, uint32_t bud_rx_index, uint32_t bud_rx_signal_index,
                                               uint32_t bud_tx_index, uint32_t bud_tx_signal_index);
bool bl00mbox_channel_disconnect_signal_rx(int32_t channel, uint32_t bud_rx_index, uint32_t bud_rx_signal_index);
bool bl00mbox_channel_disconnect_signal_tx(int32_t channel, uint32_t bud_tx_index, uint32_t bud_tx_signal_index);
bool bl00mbox_channel_disconnect_signal(int32_t channel, uint32_t bud_tx_index, uint32_t bud_tx_signal_index);
bool bl00mbox_channel_disconnect_signal_from_output_mixer(int32_t channel, uint32_t bud_index, uint32_t bud_signal_index);

bl00mbox_bud_t * bl00mbox_channel_new_bud(int32_t channel, uint32_t id, uint32_t init_var);
bool bl00mbox_channel_delete_bud(int32_t channel, uint32_t bud_index);
bool bl00mbox_channel_bud_exists(int32_t channel, uint32_t bud_index);
char * bl00mbox_channel_bud_get_name(int32_t channel, uint32_t bud_index);
char * bl00mbox_channel_bud_get_description(int32_t channel, uint32_t bud_index);
uint32_t bl00mbox_channel_bud_get_plugin_id(int32_t channel, uint32_t bud_index);
uint32_t bl00mbox_channel_bud_get_init_var(int32_t channel, uint32_t bud_index);
uint16_t bl00mbox_channel_bud_get_num_signals(int32_t channel, uint32_t bud_index);

char * bl00mbox_channel_bud_get_signal_name(int32_t channel, uint32_t bud_index, uint32_t bud_signal_index);
int8_t bl00mbox_channel_bud_get_signal_name_multiplex(int32_t channel, uint32_t bud_index, uint32_t bud_signal_index);
char * bl00mbox_channel_bud_get_signal_description(int32_t channel, uint32_t bud_index, uint32_t bud_signal_index);
char * bl00mbox_channel_bud_get_signal_unit(int32_t channel, uint32_t bud_index, uint32_t bud_signal_index);
bool bl00mbox_channel_bud_get_always_render(int32_t channel, uint32_t bud_index);
bool bl00mbox_channel_bud_set_always_render(int32_t channel, uint32_t bud_index, bool value);
bool bl00mbox_channel_bud_set_signal_value(int32_t channel, uint32_t bud_index, uint32_t bud_signal_index, int16_t value);
int16_t bl00mbox_channel_bud_get_signal_value(int32_t channel, uint32_t bud_index, uint32_t bud_signal_index);
uint32_t bl00mbox_channel_bud_get_signal_hints(int32_t channel, uint32_t bud_index, uint32_t bud_signal_index);
uint16_t bl00mbox_channel_subscriber_num(int32_t channel, uint64_t bud_index, uint16_t signal_index);
uint64_t bl00mbox_channel_get_bud_by_subscriber_list_pos(int32_t channel, uint64_t bud_index,
                uint16_t signal_index, uint8_t pos);
int32_t bl00mbox_channel_get_signal_by_subscriber_list_pos(int32_t channel, uint64_t bud_index,
                uint16_t signal_index, uint8_t pos);
uint64_t bl00mbox_channel_get_source_bud(int32_t channel, uint64_t bud_index, uint16_t signal_index);
uint16_t bl00mbox_channel_get_source_signal(int32_t channel, uint64_t bud_index, uint16_t signal_index);

bool bl00mbox_channel_bud_set_table_value(int32_t channel, uint32_t bud_index, uint32_t table_index, int16_t value);
int16_t bl00mbox_channel_bud_get_table_value(int32_t channel, uint32_t bud_index, uint32_t table_index);
uint32_t bl00mbox_channel_bud_get_table_len(int32_t channel, uint32_t bud_index);
int16_t * bl00mbox_channel_bud_get_table_pointer(int32_t channel, uint32_t bud_index);
