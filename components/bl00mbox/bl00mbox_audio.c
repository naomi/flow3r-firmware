//SPDX-License-Identifier: CC0-1.0
#include "bl00mbox_audio.h"
#include "bl00mbox_ll.h"
#include "bl00mbox_user.h"
#include "bl00mbox_os.h"

static bool is_initialized = false;
static uint16_t full_buffer_len;

static uint32_t render_pass_id;

int16_t * bl00mbox_line_in_interlaced = NULL;

static int32_t free_chan_index = 0; // increments
static bl00mbox_ll_t * all_chans = NULL;
static bl00mbox_ll_t * background_mute_override_chans = NULL;
static bl00mbox_channel_t * foreground_chan = NULL;
static bl00mbox_lock_t render_lock = NULL;

static bl00mbox_channel_t * cached_chan = NULL;

bl00mbox_channel_t * bl00mbox_get_channel(int32_t channel_index){
    if(channel_index < 0) return NULL;
    if(cached_chan && (cached_chan->index == channel_index)){
        return cached_chan;
    }
    bl00mbox_ll_t * chll = all_chans;
    while(chll){
        bl00mbox_channel_t * chan = chll->content;
        if(chan->index == channel_index){
            cached_chan = chan;
            return chan;
        }
        chll = chll->next;
    }
    return NULL;
}

bool bl00mbox_get_channel_exists(int32_t channel_index){
    return (bool) bl00mbox_get_channel(channel_index);
}

int32_t bl00mbox_get_channel_index_positional_all_chans(int32_t position){
    bl00mbox_ll_t * chll = all_chans;
    if(!chll) return -1;
    while(position){
        position--;
        chll = chll->next;
        if(!chll) return -1;
    }
    bl00mbox_channel_t * chan = chll->content;
    return chan->index;
}

int32_t bl00mbox_get_channel_index_positional_background_mute_override_chans(int32_t position){
    bl00mbox_ll_t * chll = background_mute_override_chans;
    if(!chll) return -1;
    while(position){
        position--;
        chll = chll->next;
        if(!chll) return -1;
    }
    bl00mbox_channel_t * chan = chll->content;
    return chan->index;
}

bool bl00mbox_channel_set_background_mute_override(int32_t channel_index, bool enable){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(channel_index);
    if(!ch) return false;
    if(enable){
        bl00mbox_ll_prepend(&background_mute_override_chans, ch, &render_lock);
    } else {
        bl00mbox_ll_pop(&background_mute_override_chans, ch, &render_lock);
    }
    return true;
}

bool bl00mbox_channel_get_background_mute_override(int32_t channel_index){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(channel_index);
    if(!ch) return false;
    return bl00mbox_ll_contains(&background_mute_override_chans, ch);
}

int32_t bl00mbox_channel_get_foreground_index(){
    if(foreground_chan) return foreground_chan->index;
    return -1;
}

void bl00mbox_channel_set_foreground_index(int32_t channel_index){
    bl00mbox_channel_t * chan = bl00mbox_get_channel(channel_index);
    if(!chan) return;
    if(foreground_chan != chan){
        bl00mbox_take_lock(&render_lock);
        foreground_chan = chan;
        bl00mbox_give_lock(&render_lock);
    }
}

void bl00mbox_channel_event(int32_t index){
#ifdef BL00MBOX_AUTO_FOREGROUNDING
    bl00mbox_channel_set_foreground_index(index);
#endif
}

static bl00mbox_channel_t * _bl00mbox_channel_create(){
    if(free_chan_index < 0) return NULL;
    bl00mbox_channel_t * chan = calloc(1, sizeof(bl00mbox_channel_t));
    if(!chan) return NULL;
    if(!bl00mbox_ll_prepend(&all_chans, chan, NULL)){
        free(chan);
        return NULL;
    }
    chan->volume = BL00MBOX_DEFAULT_CHANNEL_VOLUME;
    chan->sys_gain = 4096;
    chan->is_active = true;
    chan->index = free_chan_index;
    free_chan_index += 1; 

    bl00mbox_create_lock(&chan->render_lock);
    bl00mbox_take_lock(&render_lock);
    foreground_chan = chan;
    bl00mbox_give_lock(&render_lock);
    return chan;
}
bl00mbox_channel_t * bl00mbox_channel_create(){
    bl00mbox_channel_t * chan = _bl00mbox_channel_create();
    if(!chan) bl00mbox_log_error("channel allocation failed");
    return chan;
}

void  bl00mbox_channel_delete(bl00mbox_channel_t * chan){
    if(!chan) return;
    if(cached_chan == chan) cached_chan = NULL;
    if(foreground_chan == chan){
        bl00mbox_take_lock(&render_lock);
        foreground_chan = NULL;
        bl00mbox_give_lock(&render_lock);
    }
    bl00mbox_ll_pop(&background_mute_override_chans, chan, &render_lock);
    bl00mbox_ll_pop(&all_chans, chan, NULL);
    bl00mbox_channel_clear(chan->index);
    // be really sure that nobody else holds the lock. the renderer
    // doesn't at this point, but if there's multiple tasks running
    // clients there may be collisions.
    // since the client api is generally not thread safe at this point
    // it's okay, we can add the feature easily by just wrapping _all_
    // client api in a lock at some point in the future.
    bl00mbox_delete_lock(&chan->render_lock);
    free(chan->name);
    free(chan);
}

bool bl00mbox_channel_get_free(int32_t channel_index){
    // TODO: deprecate
    bl00mbox_channel_t * chan = bl00mbox_get_channel(channel_index);
    return !chan;
}

bool bl00mbox_channel_set_free(int32_t channel_index, bool set_free){
    // TODO: deprecate
    bl00mbox_channel_t * chan = bl00mbox_get_channel(channel_index);
    if(!chan) return false;
    if(set_free) bl00mbox_channel_delete(chan);
    return true;
}

int32_t bl00mbox_channel_get_free_index(){
    bl00mbox_channel_t * chan = bl00mbox_channel_create();
    if(chan) return chan->index;
    return -1;
}


char * bl00mbox_channel_get_name(int32_t channel_index){
    bl00mbox_channel_t * chan = bl00mbox_get_channel(channel_index);
    if(!chan) return NULL;
    return chan->name;
}

void bl00mbox_channel_set_name(int32_t channel_index, char * new_name){
    bl00mbox_channel_t * ch =  bl00mbox_get_channel(channel_index);
    if(!ch) return;
    if(ch->name != NULL) free(ch->name);
    ch->name = strdup(new_name);
}

void bl00mbox_channel_enable(int32_t chan){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(chan);
    if(!ch) return;
    ch->is_active = true;
}

void bl00mbox_channel_disable(int32_t chan){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(chan);
    if(!ch) return;
    ch->is_active = false;
}

void bl00mbox_channel_set_compute_mean_square(int32_t chan, bool compute){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(chan);
    if(!ch) return;
    ch->compute_mean_square = compute;
    if(!compute) ch->mean_square = 0;
}

bool bl00mbox_channel_get_compute_mean_square(int32_t chan){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(chan);
    if(!ch) return 0;
    return ch->compute_mean_square;
}

uint32_t bl00mbox_channel_get_mean_square(int32_t chan){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(chan);
    if(!ch) return 0;
    return ch->mean_square;
}

void bl00mbox_channel_set_sys_gain(int32_t chan, int16_t volume){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(chan);
    if(!ch) return;
    ch->sys_gain = volume;
}

int16_t bl00mbox_channel_get_sys_gain(int32_t chan){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(chan);
    if(!ch) return 0;
    return ch->sys_gain;
}

void bl00mbox_channel_set_volume(int32_t chan, uint16_t volume){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(chan);
    if(!ch) return;
    ch->volume = volume < 32767 ? volume : 32767;
}

int16_t bl00mbox_channel_get_volume(int32_t chan){
    bl00mbox_channel_t * ch = bl00mbox_get_channel(chan);
    if(!ch) return 0;
    return ch->volume;
}

void bl00mbox_audio_bud_render(bl00mbox_bud_t * bud){
    if(bud->render_pass_id == render_pass_id) return;
#ifdef BL00MBOX_LOOPS_ENABLE
    if(bud->is_being_rendered) return;
#endif
    bud->is_being_rendered = true;
    bud->plugin->render(bud->plugin, full_buffer_len, render_pass_id);
    bud->render_pass_id = render_pass_id;
    bud->is_being_rendered = false;
}

static bool _bl00mbox_audio_channel_render(bl00mbox_channel_t * chan, int16_t * out, bool adding){
    chan->render_pass_id = render_pass_id;

    bl00mbox_bud_list_t * always = chan->always_render;
    while(always != NULL){
        bl00mbox_audio_bud_render(always->bud);
        always = always->next;
    }

    bl00mbox_channel_root_t * root = chan->root_list;

    int32_t vol = radspa_mult_shift(chan->volume, chan->sys_gain);

    // early exit when no sources or muted:
    if((root == NULL) || (!vol)){
        return false;
    }

    int32_t acc[full_buffer_len];
    bool acc_init = false;

    while(root != NULL){
        bl00mbox_audio_bud_render(root->con->source_bud);
        if(root->con->buffer[1] == -32768){
            if(!acc_init){
                for(uint16_t i = 0; i < full_buffer_len; i++){
                    acc[i] = root->con->buffer[0];
                }
                acc_init = true;
            } else if(root->con->buffer[0]){
                for(uint16_t i = 0; i < full_buffer_len; i++){
                    acc[i] += root->con->buffer[0];
                }
            }
        } else {
            if(!acc_init){
                for(uint16_t i = 0; i < full_buffer_len; i++){
                    acc[i] = root->con->buffer[i];
                }
                acc_init = true;
            } else {
                for(uint16_t i = 0; i < full_buffer_len; i++){
                    acc[i] += root->con->buffer[i];
                }
            }
        }
        root = root->next;
    }

    for(uint16_t i = 0; i < full_buffer_len; i++){
        // flip around for rounding towards zero/mulsh boost
        bool invert = chan->dc < 0;
        if(invert) chan->dc = -chan->dc;
        chan->dc = ((uint64_t) chan->dc * (((1<<12) - 1)<<20)) >> 32;
        if(invert) chan->dc = -chan->dc;

        chan->dc += acc[i];

        acc[i] -= (chan->dc >> 12);
    }
    if(adding){
        for(uint16_t i = 0; i < full_buffer_len; i++){
            out[i] = radspa_add_sat(radspa_gain(acc[i], vol), out[i]);
        }
    } else {
        for(uint16_t i = 0; i < full_buffer_len; i++){
            out[i] = radspa_gain(acc[i], vol);
        }
    }
    if(chan->compute_mean_square){
        for(uint16_t i = 0; i < full_buffer_len; i++){
            int32_t sq = acc[i];
            sq = (sq * sq) - chan->mean_square;
            // always round down with negative sq so that decay always works.
            // bitshift instead of div does that for us nicely.
            // cannot underflow as ((-a) >> 11) can never be less than -a.
            chan->mean_square += sq >> 11;
        }
    }
    return true;
}

static bool bl00mbox_audio_channel_render(bl00mbox_channel_t * chan, int16_t * out, bool adding){
    if(!chan) return false;
    if(!chan->is_active) return false;
    if(render_pass_id == chan->render_pass_id) return false;
    bl00mbox_take_lock(&chan->render_lock);
    bool ret = _bl00mbox_audio_channel_render(chan, out, adding);
    bl00mbox_give_lock(&chan->render_lock);
    return ret;
}

static bool _bl00mbox_audio_render(int16_t * rx, int16_t * tx, uint16_t len){
    if(!is_initialized) return false;

    render_pass_id++; // fresh pass, all relevant sources must be recomputed
    full_buffer_len = len/2;
    bl00mbox_line_in_interlaced = rx;
    int16_t acc[full_buffer_len];
    bool acc_init = false;

    bl00mbox_take_lock(&render_lock);

    // re-rendering channels is ok, if render_pass_id didn't change it will just exit
    acc_init = bl00mbox_audio_channel_render(foreground_chan, acc, acc_init) || acc_init;
    bl00mbox_ll_t * chll = background_mute_override_chans;
    while(chll){
        acc_init = bl00mbox_audio_channel_render(chll->content, acc, acc_init) || acc_init;
        chll = chll->next;
    }

    bl00mbox_give_lock(&render_lock);

    if(!acc_init) return false;

    for(uint16_t i = 0; i < full_buffer_len; i++){
        tx[2*i] = acc[i];
        tx[2*i+1] = acc[i];
    }
    return true;
}

void bl00mbox_audio_render(int16_t * rx, int16_t * tx, uint16_t len){
    if(!_bl00mbox_audio_render(rx, tx, len)) memset(tx, 0, len*sizeof(int16_t));
}

void bl00mbox_audio_init(){
    if(render_lock) abort();
    bl00mbox_create_lock(&render_lock);
    if(!render_lock) abort();
    is_initialized = true;
}
