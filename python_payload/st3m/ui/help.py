from st3m import Responder
from st3m.utils import wrap_text


class Help(Responder):
    def __init__(self, inputcontroller, help_text):
        self.input = inputcontroller
        self.input.buttons.os.left.repeat_enable(500, 300)
        self.input.buttons.os.left.repeat_enable(500, 300)
        self.help_text = help_text
        self.override_os_button_back = False
        self.line_height = 16
        self.line_width = 190
        self.line_height_steps = 3
        self.x = -self.line_width / 2
        self.y = -80
        self.lines = None
        self.full_redraw = True

    def think(self, ins, delta_ms):
        if self.lines is None:
            return
        ud_dir = self.input.buttons.app.right.pressed
        ud_dir -= self.input.buttons.app.left.pressed
        ud_dir += self.input.buttons.app.right.repeated
        ud_dir -= self.input.buttons.app.left.repeated
        if ud_dir:
            self.full_redraw = True
        self.y += -ud_dir * self.line_height * self.line_height_steps
        if self.y > -80:
            self.y = -80
        min_y = -80 - self.line_height * max(len(self.lines) - 8, 0)
        if self.y < min_y:
            self.y = min_y

    def draw(self, ctx):
        if self.full_redraw:
            ctx.rgb(0, 0, 0)
            ctx.rectangle(-120, -120, 240, 240).fill()
            ctx.rgb(0x81 / 255, 0xCD / 255, 0xC6 / 255)
            ctx.move_to(0, self.y)
            ctx.text_align = ctx.CENTER
            if not (self.y < -120):
                ctx.font_size = 24
                ctx.text("~ help ~")
            ctx.font_size = 16
            offset = self.line_height
            if not isinstance(self.help_text, str):
                offset += self.line_height
                ctx.move_to(0, self.y + offset)
                ctx.text("no help found :/")
                return
            ctx.text_align = ctx.LEFT
            if self.lines is None:
                self.lines = wrap_text(self.help_text, self.line_width, ctx)
            num_lines = len(self.lines)
            x = 0
            while x < num_lines:
                line = self.lines[x]
                offset += self.line_height
                diff = -120 - self.y - offset
                if diff > 0:
                    jump = max(1, diff // self.line_height)
                    offset += self.line_height * (jump - 1)
                    x += jump
                    continue
                elif (self.y + offset) > (120 + self.line_height):
                    break
                if line:
                    ctx.move_to(self.x, self.y + offset)
                    ctx.text(line)
                x += 1

    def on_exit(self):
        pass
